import firebase from 'firebase';
var config = {
  apiKey: "AIzaSyAnuKPyT6tKiC6NANMiqh44MYBZ6s2O_cc",
  authDomain: "topsy-3c19c.firebaseapp.com",
  databaseURL: "https://topsy-3c19c.firebaseio.com",
  projectId: "topsy-3c19c",
  storageBucket: "topsy-3c19c.appspot.com",
  messagingSenderId: "270654049466"
};
const firebaseApp = firebase.initializeApp(config);
const db = firebaseApp.database();
export default db;
