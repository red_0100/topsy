import Vue from 'vue';
import Q from 'q';

export default new Vue({
  data: {
    uid: null
  },
  methods: {
    //i was hoping to call in route change so every page change would make sure im logged in
    // its causing super minor issues
    set(){
      let deferred = Q.defer();

      this.uid = this.$cookie.get('topsy-user');
      console.log('Login UID:', this.uid);

      if(this.uid){
        deferred.resolve(this.uid);
      }else{
        var provider = new firebase.auth.FacebookAuthProvider();
        provider.addScope('email');
        provider.addScope('user_friends');
        provider.addScope('public_profile');
        let signIn = firebase.auth().signInWithPopup(provider);
        signIn.then((result) => {
          // This gives you a Facebook Access Token. You can use it to access the Facebook API.
          var token = result.credential.accessToken;
          // The signed-in user info.
          var user = result.user;
          console.log(result);

          this.$cookie.set('topsy-user', user.uid);
          deferred.resolve(user.uid);
        }).catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // The email of the user's account used.
          var email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          var credential = error.credential;

          console.log(error);
          deferred.reject(error);
        });
      }

      return deferred.promise;
    },
    unset(){
      firebase.auth().signOut().then(() => {
        this.$cookie.delete('topsy-user');
        this.router.push('login');
      });
    },
  }
});


/* https://firebase.google.com/docs/auth/web/auth-state-persistence
firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
  .then(function() {
    // Existing and future Auth states are now persisted in the current
    // session only. Closing the window would clear any existing state even
    // if a user forgets to sign out.
    // ...
    // New sign-in will be persisted with session persistence.
    return firebase.auth().signInWithEmailAndPassword(email, password);
  })
  .catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
  });
*/
