import Vue from 'vue';
import Q from 'q';

export default {
    getReviews(currentLocation){
      this.placesService.getDetails({ placeId: currentLocation.place_id }, (place, status) => {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
          currentLocation.reviews = place.reviews;
        }
      });
    },
    setInfoWindow(name, marker){
      const infoWindow = new google.maps.InfoWindow({
        content: `<span>${name}</span>`,
        maxWidth: 250
      });

      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.open(this.map, marker);
      });

    },
    setMarker(name, latLng){
      const marker = new google.maps.Marker({
        position: latLng,
        map: this.map,
      });

      this.setInfoWindow(name, marker);
    },
    setCircle(latLng){
      return new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: this.map,
        center: latLng,
        radius: 1000
      });
    },
    load(id, config){
      console.log('MAP ', id);
      const deferred = Q.defer();
      const mapId = document.getElementById(id);

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          let currentLocation = position.coords;

          let latLng = {
            lat: currentLocation.latitude,
            lng: currentLocation.longitude
          };

          this.map = new google.maps.Map(mapId, {
            zoom: 11,
            center: latLng
          });

          this.setMarker(latLng);
          this.setCircle(latLng);

          console.log(currentLocation);
          this.currentLocation = latLng;
          let request = config || {
            radius: 500,
            location: new google.maps.LatLng( currentLocation.latitude, currentLocation.longitude ),
            type: ['restaurant', 'movie_theater', 'bar', 'food']
          };

          this.placesService = new google.maps.places.PlacesService(this.map);
          this.placesService.nearbySearch(request, (data) => {
            deferred.resolve(data);
          });
        });
      }

      return deferred.promise;
    }
  };
