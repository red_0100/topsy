import Vue from 'vue';
import VueCookie from 'vue-cookie';
import VueFire from 'vuefire';

import './app.scss';
import VueRouter from 'vue-router';
import Main from './main/main.vue';
import Events from './events/events.vue';
import EventSetup from './event-setup/event-setup.vue';
// import Login from './login/login.vue';

Vue.use(VueRouter);
Vue.use(VueCookie);
Vue.use(VueFire);

const router = new VueRouter({
  mode: 'history',
  routes: [
    // {
    //   name: 'login',
    //   component: Login,
    //   path: '/login'
    // },
    {
      path: '/',
      component: Main,
      children: [
        {
          name: 'events',
          component: Events,
          path: 'events',
        },
        {
          name: 'event-setup',
          component: EventSetup,
          path: 'event/:eventId?',
          props: true
        },
        {
          path: '*',
          redirect: 'events'
        }
      ]
    },
  ]
});

new Vue({
  router,
  template: `<router-view></router-view>`,
  beforeMount(){
    //not sure correct way to hold reference router from login
    // LoginStore.router = router;
    // THis is a problem because event store loads with out fb cookie  being set.
    // thats why it doesnt loads data right away but it does on refresh
    // LoginStore.set();
  }
}).$mount('#app');
