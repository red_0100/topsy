import Q from 'q';
import map from '../share/map';
import db from '../share/database';

const userId = 'wsVHA8AEa8hpMcYztXmEovRS4jY2';
const userRef = db.ref(`users/${userId}`);
const userEventRef = userRef.child('events');
const eventsRef = db.ref('events');

const mapStore = map;
console.log(mapStore);


export default {
  name: 'event-setup',
  props: ['eventId'],
  firebase: {
    user: userRef,
    events: eventsRef
  },
  data () {
    return {
      eventProperties: {
        invitees: {},
        venues: '',
        status: 'pending',
        name: 'test',
        timeStart: '',
        timeEnd: '',
        dateStart: '',
        dateEnd: '',
      },
      locationData: [],
      invitees: [],
      venues: [],
      filterList: ['restaurant', 'movie_theater', 'bar', 'food']
    };
  },
  methods: {
    toggleFilters(filter){
      console.log(filter);
    },
    loadMap(){

      mapStore.load('map').then(  venues => {
        this.venues = venues;

        venues.forEach( venue => {
          let { lat, lng } = venue.geometry.location;

          mapStore.setMarker( venue.name, {
            lat: lat(),
            lng: lng()
          });

          mapStore.getReviews(venue);
        });

        this.saveUserLocation();
      });
    },
    /*
    saveUserLocation(){
      const fb = new Firebase(`events/${this.eventId}/invitees/${LoginStore.uid}`);
      fb.set(mapStore.currentLocation);
    },
    */
    setupEvent(){
      const dbEventData = eventsRef.push(this.eventProperties);
      const dbUsers = userEventRef.push(dbEventData.key);
      dbUsers.set('pending');
      return dbEventData.key;
    },
    findEvent(){
      const deferred = Q.defer();
      let eventResult = {};

      if(this.eventId){
        eventResult.eventId = this.eventId;

        eventsRef.child(this.eventId).once('value').then(data => {
          eventResult = data.val();
          if(!eventResult){
            this.setupEvent();
          }else{
            //maybe ask to create new event.
            console.log(eventResult);
          }
          deferred.resolve(eventResult);
        });

      }else{
        const eventId = this.setupEvent();
        this.$router.push({ name: 'event-setup', params: { eventId: eventId }});
        eventResult.eventId = eventId;
        deferred.resolve(eventResult);
      }
      return deferred.promise;
    }
  },
  mounted(){
    this.findEvent().then( this.loadMap );
  }
};
