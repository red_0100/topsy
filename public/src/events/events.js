//https://medium.com/@anas.mammeri/vue-2-firebase-how-to-build-a-vue-app-with-firebase-authentication-system-in-15-minutes-fdce6f289c3c
import db from '../share/database';
let userId = 'wsVHA8AEa8hpMcYztXmEovRS4jY2';
let userRef = db.ref(`users/${userId}`);
let eventsRef = userRef.child('events');

export default {
  name: 'home',
  firebase: {
    user: userRef,
    events: eventsRef
  },
  data () {
    return {
    };
  },
  methods: {
    removeEvent: function (key) {
      // itemsRef.child(key).remove();
    },
    addEvent: function () {
      // if (this.newTodo.trim()) {
      //   itemsRef.push({
      //     text: this.newTodo
      //   });
      //   this.newTodo = "";
      // }
    },
    getEvents(){
      console.log('FIREBASE', this, this.events);
      // eventsRef.once('value').then( snapshot => {
      //   console.log(snapshot);
      //   this.events = snapshot.val();
      // });
    }
  },
  mounted(){
    this.getEvents();
  }
};
