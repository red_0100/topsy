# topsy

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# deploy
npm run firebase deploy
```
*note: To deploy change the build.js in the index.html file from dist/build.js to build.js.




For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
http://firebase.googleblog.com/2016/04/vuefire-firebase-meets-vuejs_0.html

```
       .----.-.
      /    ( o \
     '|  __ ` ||
      |||  ||| -'

```

